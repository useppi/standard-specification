<Description Name="gallery">

    <Image>http://www.example.com/my-beautiful-room.jpg</Image>
    <Text TextFormat="PlainText" Language="en">Description EN</Text>
    <Text TextFormat="PlainText" Language="it">Description IT</Text>
    <Text TextFormat="PlainText" Language="de">Description DE</Text>
    <Text TextFormat="PlainText">(C) 2012 Example Inc.</Text>
    <URL>http://www.example.com/attribution/</URL>

    <Image>http://www.example.com/my-even-more-beautiful-room.jpg</Image>
    <Text TextFormat="PlainText" Language="en">Description EN</Text>
    <Text TextFormat="PlainText" Language="it">Description IT</Text>
    <Text TextFormat="PlainText" Language="de">Description DE</Text>
    <Text TextFormat="PlainText">(C) 2012 Example Inc.</Text>
    <URL>http://www.example.com/attribution/</URL>

    <!-- ... -->

</Description>
